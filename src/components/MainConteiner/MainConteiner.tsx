import React, { useState } from 'react'
import { useIonViewDidEnter, IonProgressBar } from '@ionic/react'
import WeatherConteiner from '../WeatherConteiner/WeatherConteiner'
import ForecastConteiner from '../ForecastConteiner/ForecastConteiner'
import { useHistory } from 'react-router'
import { getUrl } from '../../utils/urls'
import './MainConteiner.css'

interface ContainerProps {
    type: 'weather' | 'forecast'
}

const MainConteiner: React.FC<ContainerProps> = ({ type }) => {
    const [resData, setResData] = useState({})
    const [error, setError] = useState('')
    const [isLoaded, setIsLoaded] = useState(false)
    const history = useHistory()

    useIonViewDidEnter(() => {
        downloadData()
    })

    function downloadData() {
        const url = type === 'weather' ? getUrl('weather') : getUrl('forecast')
        if (!url) {
            history.push('/settings')

        } else {
            fetch(url)
                .then(res => res.json())
                .then(data => {
                    // eslint-disable-next-line eqeqeq
                    data.cod != 200 ? setError(data.message) : setResData(data)  
                })
                .catch(err => setError(err))
                .then(() => setIsLoaded(true))
        }
    }

    if (error) {
        return (<div className='mainConteiner'><p>Unfortunately we were unable to download data</p></div>)

    } else if (!isLoaded) {
        return (<IonProgressBar type="indeterminate"></IonProgressBar>)

    } else {
        return type === 'weather' ? <WeatherConteiner data={resData}/> : <ForecastConteiner data={resData} />
    }
}

export default MainConteiner
