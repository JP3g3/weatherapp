import React, { useState } from 'react'
import { IonInput, IonButton, useIonViewDidEnter } from '@ionic/react'
import { getLocation, setLocation } from '../../utils/localStorage'
import { getTestUrl } from '../../utils/urls'
import { useHistory } from 'react-router'
import './FindLocationForm.css'

const FindLocationForm: React.FC = () => {
    const [value, setValue] = useState('')
    const [disableForm, setDisable] = useState(false)
    const [response, setResponse] = useState('')
    const history = useHistory()

    useIonViewDidEnter(() => {
        const location = getLocation()
        if (location) { setValue(location) }
        setResponse('Search')
    })

    const handleSubmit = () => {
        if (value.length > 1) {
            setDisable(true)
            validateData()
        }
    }

    const validateData = () => {
        const url = getTestUrl(value)
        fetch(url)
            .then(res => res.json())
            .then(data => {
                if (data.cod === 200) {
                    setResponse('Saved')
                    setLocation(value)
                    history.push('/weather')
                } else setResponse('Not found')
            }).catch(err => setResponse('something went wrong'))
            .then(() => setDisable(false))
    }

    return (
        <div className="formContainer">
            <h2>Write your city name</h2>
            <IonInput
                disabled={disableForm}
                value={value}
                placeholder="e.g. Warsaw"
                onIonChange={e => setValue(e.detail.value!)}>
            </IonInput>
            <IonButton
                disabled={disableForm}
                onClick={handleSubmit}>
                {response}
            </IonButton>
        </div>
    )
}

export default FindLocationForm