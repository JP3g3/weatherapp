import React from 'react'
import { IonList, IonItem, IonLabel, IonItemDivider } from '@ionic/react'
import { toCelcius, getTime, getDay, getDate } from '../../utils/showData'
import { iconUrl } from '../../utils/urls'

const ForecastConteiner: React.FC<any> = ({ data }) => {
    const { list } = data
    let day: number

    const ForecastList = list.map((value: any) => {
        const { dt, main, weather } = value
        let currentDay = getDay(dt)
        let divider = null

        if (!day || currentDay !== day) {
            divider = (
                <IonItemDivider>
                    <IonLabel>
                        { getDate(dt) }
                    </IonLabel>
                </IonItemDivider>
            )
        }

        day = currentDay

        return (
            <React.Fragment key={dt}>
                { divider }
                <IonItem key={dt}>
                    <IonLabel>{getTime(dt)}</IonLabel>
                    <IonLabel>{toCelcius(main.temp)}&deg;C</IonLabel>
                    <img alt='weather icon' src={`${iconUrl}${weather[0].icon}.png`}></img>

                </IonItem>
            </React.Fragment>
        )
    })

    return (
        <IonList>
            { ForecastList }
        </IonList>
    )
}

export default ForecastConteiner