import React from 'react'
import { IonLabel, IonItem, IonCol, IonList } from '@ionic/react'
import { toCelcius, getTime } from '../../utils/showData'
import { iconUrl } from '../../utils/urls'
import './WeatherConteiner.css'

const WeatherConteiner: React.FC<any> = ({ data }) => {
    const { dt, main, name, sys, weather, wind } = data
    const { humidity, pressure, temp, temp_max, temp_min } = main
    const { sunrise, sunset, country } = sys
    const [{ description: desc, icon }] = weather
    const { speed, deg } = wind

    return (
        <div className="weatherConteiner">
            <div className="header">
                <h5>{name}, {country}</h5>
                <img alt='weather iocn' src={`${iconUrl}${icon}.png`} width='100' height='100'></img>
                <h1>{toCelcius(temp)}&deg;C</h1>
                <h4>{desc}</h4>
                <h6>Updated at {getTime(dt)}</h6>
            </div>

            <IonList mode='ios'>
                <IonItem>
                    <IonCol>
                        <IonLabel>Temperature</IonLabel>
                    </IonCol>
                    <IonCol>
                        <IonLabel>{toCelcius(temp_min)}&deg;C / {toCelcius(temp_max)}&deg;C</IonLabel>
                    </IonCol>
                </IonItem>
                <IonItem>
                    <IonCol>
                        <IonLabel>Sunrise</IonLabel>
                    </IonCol>
                    <IonCol>
                        <IonLabel>{getTime(sunrise)}</IonLabel>
                    </IonCol>
                </IonItem>
                <IonItem>
                    <IonCol>
                        <IonLabel>Sunset</IonLabel>
                    </IonCol>
                    <IonCol>
                        <IonLabel>{getTime(sunset)}</IonLabel>
                    </IonCol>
                </IonItem>
                <IonItem>
                    <IonCol>
                        <IonLabel>Humidity</IonLabel>
                    </IonCol>
                    <IonCol>
                        <IonLabel>{humidity} %</IonLabel>
                    </IonCol>
                </IonItem>
                <IonItem>
                    <IonCol>
                        <IonLabel>Pressure</IonLabel>
                    </IonCol>
                    <IonCol>
                        <IonLabel>{pressure} hPa</IonLabel>
                    </IonCol>
                </IonItem>
                <IonItem>
                    <IonCol>
                        <IonLabel>Wind</IonLabel>
                    </IonCol>
                    <IonCol>
                        <IonLabel>speed {speed} m/s</IonLabel><br />
                        <IonLabel>direction {deg}&deg;</IonLabel>
                    </IonCol>
                </IonItem>
            </IonList>
        </div>
    )
}

export default WeatherConteiner