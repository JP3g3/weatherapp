import React from 'react'
import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar } from '@ionic/react'
import MainConteiner from '../components/MainConteiner/MainConteiner'


const Weather: React.FC = () => {
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Today's weather</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <MainConteiner type='weather'/>
      </IonContent>
    </IonPage>
  )
}

export default Weather
