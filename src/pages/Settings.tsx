import React from 'react'
import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar } from '@ionic/react'
import FindLocationForm from '../components/FindLocationForm/FindLocationForm'

const Settings: React.FC = () => {
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Settings</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <FindLocationForm />
      </IonContent>
    </IonPage>
  )
}

export default Settings
