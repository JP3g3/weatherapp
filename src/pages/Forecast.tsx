import React from 'react'
import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar } from '@ionic/react'
import MainConteiner from '../components/MainConteiner/MainConteiner'

const Forecast: React.FC = () => {
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Forecast for the next hours</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <MainConteiner type='forecast' />
      </IonContent>
    </IonPage>
  )
}

export default Forecast
