import { getLocation } from './localStorage'

const APIkey = '671c4b7a8ac73bc13ce472c500930520'
const baseUrl = 'http://api.openweathermap.org/data/2.5/'
const iconUrl = 'https://openweathermap.org/img/wn/'

const getUrl = (type: 'weather' | 'forecast') => {
    const location = getLocation()
    if (location) {
        return `${baseUrl}${type}?q=${location}&appid=${APIkey}`
    } else return ''
}

const getTestUrl = (location: string) => {
    return `${baseUrl}weather?q=${location}&appid=${APIkey}`
}

export { getUrl, getTestUrl, iconUrl }