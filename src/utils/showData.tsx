
const toCelcius = (kelvin: number) => (kelvin - 273.15).toFixed(1)

const getTime = (time: number) => {
    const date = new Date(time * 1000)
    const addZero = (dt: number) => (dt < 10 ? '0' : '') + dt;
    const hour = addZero(date.getHours())
    const minutes = addZero(date.getMinutes())
    return `${hour}:${minutes}`
}

const getDay = (time: number) => {
    const date = new Date(time * 1000)
    return date.getDate()
}

const getDate = (time: number) => {
    const date = new Date(time * 1000)
    return date.toDateString().slice(0, -5)
}

export { toCelcius, getTime, getDay, getDate }