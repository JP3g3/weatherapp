const myKey = 'myLocation'

const getLocation = () => {
    return localStorage.getItem(myKey)
}

const setLocation = (location: string) => {
    localStorage.setItem(myKey, location)
}

export { getLocation, setLocation } 